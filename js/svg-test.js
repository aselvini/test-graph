const lines = document.getElementById('lines');

const getBlock = function (id) {
  const node = document.getElementById(id);
  // window.console.log('ziocan ', id, node.getClientRects().top)
  return {
    x: node.offsetLeft,
    y: node.offsetTop
  }; // node.getClientRects();
}
const getSvg = function (idContainer) {
  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttribute("style", "-webkit-tap-highlight-color: rgba(0, 0, 0, 0);");
  svg.setAttribute("width", "500");
  svg.setAttribute("height", "500");
  return svg;
};

const getPath = function (id1, id2) {
  const coords1 = getBlock(id1)
  const coords2 = getBlock(id2)
  const path = document.createElementNS('http://www.w3.org/2000/svg', "path");

  // document.createElementNS('http://www.w3.org/2000/svg','path');
  path.setAttribute("fill", "none");
  path.setAttribute("stroke", "#ff0000");
  path.setAttribute("style", "-webkit-tap-highlight-color: rgba(0, 0, 0, 0);");
  path.setAttribute("d", `M${ coords1.x },${ coords1.y }L${ coords2.x },${ coords2.y }`);
  return path
}

const buildGraph = () => {
  window.console.log('buildGraph called');
  lines
    .appendChild(getSvg())
    .appendChild(getPath('block1', 'block2'));
}

export { buildGraph };