import * as d3 from "d3";

// source https://observablehq.com/@d3/spline-editor#chart

const width = 500;
const height = 500;

const points = [
 [190.8, 168.69351221628122],
 [381.6, 466.9861425088751],
 [572.4, 416.00311539850594],
 [763.2, 70.03010463762101]
];

const generateGraph = () => {
  let selected = points[0];

  const svg = d3.create("svg")
    .attr("viewBox", [-14, 0, width + 28, height])
    .attr("tabindex", 1)
    .attr("pointer-events", "all");

  svg.append("style").text(`
svg[tabindex] {
  display: block;
  margin: 0 -14px;
  border: solid 2px transparent;
  box-sizing: border-box;
}
svg[tabindex]:focus {
  outline: none;
  border: solid 2px lightblue;
}
`);

  svg.append("rect")
    .attr("fill", "none")
    .attr("width", width)
    .attr("height", height);

  svg.append("path")
    .datum(points)
    .attr("fill", "none")
    .attr("stroke", "black")
    .attr("stroke-width", 1.5)
    .call(update);

  function update() {
    // svg.select("path").attr("d", line);

    const circle = svg.selectAll("g")
      .data(points, d => d)

    circle.enter().append("g")
      .call(g => g.append("circle")
        .attr("r", 30)
        .attr("fill", "none"))
      .call(g => g.append("circle")
        .attr("r", 0)
        .attr("stroke", "black")
        .attr("stroke-width", 1.5)
        .transition()
        .duration(750)
        .ease(d3.easeElastic)
        .attr("r", 5))
      .merge(circle)
      .attr("transform", d => `translate(${d})`)
      .select("circle:last-child")
      .attr("fill", d => d === selected ? "lightblue" : "black");

    circle.exit().remove();
  }

  return svg;
}




const drawD3 = () => {
  const svg = generateGraph();
  window.console.log('d3: ', d3, svg.node());
  return  svg.node();
}
export { drawD3 }