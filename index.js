import { buildGraph } from './js/svg-test';
import { drawD3 } from './js/d3-test';

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('drawSvgGraph').addEventListener('click', buildGraph, false);
  // document.getElementById('containerTest3').appendChild(drawD3());
});

